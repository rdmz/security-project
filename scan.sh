#!/bin/bash

HOST="acx0.dyndns.org"

DEFAULT_TCP=1052    # in update.c
DEFAULT_UDP=4156    # in unlock.c

TCP=$DEFAULT_TCP
UDP=$DEFAULT_UDP

check_for_apache()
{
    HAS_WEBSERVER=0

    for (( i = 0; i < ${#PORTS[@]}; i += 3 )); do
        PORT=$(echo ${PORTS[$i]} | cut -d "/" -f 1)
        SERVICE=${PORTS[$i + 2]}

        if [[ $PORT == "80" && SERVICE == "http" ]]; then
            HAS_WEBSERVER=1
        fi
    done

    if [[ $HAS_APACHE == 0 ]]; then
        echo "http server not running on \`$HOST', attach will not work"
        exit 1
    fi
}

find_open_port()
{
    FIND_PROTO=$1

    for (( i = 0; i < ${#PORTS[@]}; i += 3 )); do
        PORT=$(echo ${PORTS[$i]} | cut -d "/" -f 1)
        PROTOCOL=$(echo ${PORTS[$i]} | cut -d "/" -f 2)
        STATUS=${PORTS[$i + 1]}
        SERVICE=${PORTS[$i + 2]}

        if [[ $PROTOCOL == $FIND_PROTO && $STATUS == "open" ]]; then
            if [[ $FIND_PROTO == "tcp" ]]; then
                TCP=$PORT
            elif [[ $FIND_PROTO == "udp" ]]; then
                UDP=$PORT
            fi
            return
        fi
    done
}

main()
{
    if [[ -n $1 ]]; then
        HOST=$1
    fi

    echo "scanning host \`$HOST'..."

    check_for_apache

    PORTS=( $(nmap -PN "$HOST" | grep "open") )

    find_open_port "tcp"
    find_open_port "udp"

    if [[ $TCP != $DEFAULT_TCP ]]; then
        sed -i "s/#define PORT.*/#define $TCP/" update.c
        echo "updated TCP port to: $TCP"
    else
        echo "using default TCP port: $DEFAULT_TCP"
    fi

    if [[ $UDP != $DEFAULT_UDP ]]; then
        sed -i "s/#define PORT.*/#define $UDP/" unlock.c
        echo "updated UDP port to: $UDP"
    else
        echo "using default UDP port: $DEFAULT_UDP"
    fi
}

main
