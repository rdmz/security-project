CC=gcc
#CC=clang
CFLAGS=-lcrypto

default: unlock update

unlock: unlock.c Makefile
	${CC} unlock.c -o unlock ${CFLAGS}

update: update.c Makefile
	${CC} update.c -o update ${CFLAGS}

clean:
	rm unlock update
